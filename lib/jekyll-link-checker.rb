# frozen_string_literal: true

require_relative "link-checker"
require_relative "jekyll-hook"
require_relative "version"
