# frozen_string_literal: true

require "jekyll"

Jekyll::Hooks.register :site, :post_write, priority: 0 do |site|
  link_checker = LinkChecker.from_config(site.config)
  link_checker.check_links
end
