# frozen_string_literal: true

require "faraday"
require "faraday_middleware"
require "faraday-cookie_jar"
require "addressable"
require "pathname"

# Checks all the links of a static website to make sure they're all valid
class LinkChecker
  HEADERS = {
    "User-Agent" => "Mozilla/5.0 (Windows NT 6.1) " \
                    "AppleWebKit/537.36 (KHTML, like Gecko) " \
                    "Chrome/41.0.2228.0 Safari/537.36",
    "Accept" => "text/html," \
                "application/xhtml+xml," \
                "application/xml;" \
                "q=0.9,*/*;q=0.8",
    "Accept-Language" => "en-US,en;q=0.5",
    "DNT" => "1",
    "Upgrade-Insecure-Requests" => "1",
    "Pragma" => "no-cache",
    "Cache-Control" => "no-cache"
  }.freeze

  HREF = /href="([^"\n]+)"/.freeze
  ID = /id="([^"\n]+)"/.freeze
  HTML = %w[.html .htm].freeze
  SCHEMES = %w[https http].freeze

  DEFAULT_BASE_URL = "/"
  DEFAULT_SITE_FOLDER = "_site"
  DEFAULT_MODE = "try_head"

  attr_accessor :hostname, :baseurl, :site_folder, :skip_list, :mode, :verbose,
                :ignore_fragments, :fail_fast, :abort_on_failure
  attr_writer :files, :html_files, :links

  # Set default values for all the properties
  def initialize(opts = {})
    @hostname = opts[:hostname]
    @baseurl = opts[:baseurl] || DEFAULT_BASE_URL
    @site_folder = opts[:site_folder] || DEFAULT_SITE_FOLDER

    update_skip_list(opts[:skip_list] || [])
    @ignore_fragments = opts[:ignore_fragments]

    @mode = opts[:mode] || DEFAULT_MODE
    @verbose = opts[:verbose]
    @fail_fast = opts[:fail_fast]

    abort_on_failure = opts[:abort_on_failure]
    @abort_on_failure = abort_on_failure.nil? ? true : abort_on_failure
  end

  # Initialize the link checker from a Jekyll configuration file
  def self.from_config(config)
    opts = {}

    url = config["url"]
    if url
      uri = Addressable::URI.parse(url)
      opts[:hostname] = uri.hostname
    end

    opts[:baseurl] = config["baseurl"]
    opts[:site_folder] = config["destination"]

    link_checker_config = config["link-checker"]
    if link_checker_config
      opts[:skip_list] = link_checker_config["skip-list"]
      opts[:ignore_fragments] = link_checker_config["ignore-fragments"]
      opts[:mode] = link_checker_config["mode"]
      opts[:verbose] = link_checker_config["verbose"]
      opts[:fail_fast] = link_checker_config["fail-fast"]
      opts[:abort_on_failure] = link_checker_config["abort"]
    end

    LinkChecker.new(opts)
  end

  # Whether the options are valid
  def valid?
    return false unless @hostname

    true
  end

  # Updates skip_list with the given argument.
  # If the argument is an array, skip_list is set to the array.
  # If the argument is a string, the argument will be interpreted as a filename
  # where each line is a filename.
  def update_skip_list(skip_list)
    if skip_list.is_a?(Array)
      @skip_list = skip_list
    elsif skip_list.is_a?(String)
      begin
        @skip_list = File.readlines(File.expand_path(skip_list)).map(&:strip)
      rescue StandardError => e
        warn "Couldn't read the skip list"
        raise e
      end
      @skip_list.reject!(&:empty?)
    else
      raise ArgumentError, "skip_list must be a String or an array of String"
    end
  end

  # Checks all the links
  def check_links
    # Make sure the configuration is valid
    abort "Invalid configuration" unless valid?

    # basepath = @baseurl ? Addressable::URI.parse(@baseurl).path : "/"

    conn = create_connection

    # Test each link
    error_count = 0
    i = 0
    prev_msg_size = 0
    links.each do |uri, fragments|
      i += 1
      if verbose
        prev_msg_size.times { print " " }
        msg = "#{uri} #{i}/#{links.size}"
        print "\r#{msg}\r"
        prev_msg_size = msg.size
      end

      # Skip the link if it's in the skip list
      next if @skip_list.include?(uri.to_s)

      error = false

      # If the link is internal
      if uri.hostname.nil? || uri.hostname == hostname
        uri.path.chomp!("/")

        # If the uri's path is valid
        valid_fragments = valid_links[uri.path]
        if valid_fragments
          fragments.each do |fragment, files|
            # Skip the base fragment
            next unless fragment

            next if valid_fragments.include?(fragment)

            error = true
            puts "Invalid fragment '#{fragment}' in link '#{uri}' " \
                 "is present in:"
            files.each { |file| puts "\t#{file}" }
          end
        else
          error = true
          puts "Invalid internal link '#{uri}' is present in:"
          fragments.flat_map { |_, files| files }.uniq
                   .each { |file| puts "\t#{file}" }
        end
      elsif fragments.keys == [nil]
        begin
          status = make_request(conn, uri)
          error = !status_allowed?(status)
          if error
            puts "Request to #{uri} returned #{status} present in"
            fragments[nil].each { |file| puts "\t#{file}" }
          end
        rescue StandardError => e
          puts "Request to #{uri} produced the error #{e.class} present in"
          fragments[nil].each { |file| puts "\t#{file}" }
          puts e.message, "\n"
        end
      else
        begin
          response = get_request(conn, uri)
          status = response.status
          if status == 200
            valid_fragments = uniq_string_matches(response.body, ID)
            fragments.each do |fragment, files|
              unless valid_fragments.include?(fragment)
                puts "Invalid link to fragment '#{fragment}' present in: "
                files.each { |file| puts "\t#{file}" }
              end
            end
          else
            error = true
            puts "Request to #{link} in #{files} returned #{status}"
            error = true
            puts "Invalid internal link '#{link}' is present in:"
            fragments.flat_map { |_, files| files }.uniq
                     .each { |file| puts "\t#{file}" }
          end
        rescue StandardError => e
          puts "Request to #{uri} produced the error #{e.class} present in"
          fragments[nil].each { |file| puts "\t#{file}" }
          puts e.message, "\n"
        end
      end

      next unless error

      error_count += 1
      if fail_fast
        abort if abort_on_failure
        return nil
      end
    end

    puts if verbose

    if error_count != 0
      msg = "There were #{error_count} invalid links"
      if @abort_on_failure
      then abort msg
      else puts msg
      end
    end

    error_count
  end

  # Find all files in the site folder
  def files
    return @files unless @files.nil?

    @files = Dir[File.join(@site_folder, "**/*")].select { |f| File.file?(f) }
    @files
  end

  # Find all the valid links for the site
  # The value returned by this method is formatted like so:
  # {
  #   "path": [
  #     fragment
  #   ]
  # }
  def valid_links
    return @valid_links if @valid_links

    @valid_links = files.map do |file|
      fragments = []
      fragments = uniq_file_matches(file, ID) if html?(file) &&
                                                 !@ignore_fragments

      [file_url(file), fragments]
    end
    @valid_links = @valid_links.to_h
  end

  # Find all HTML files
  def html_files
    return @html_files if @html_files

    @html_files = files.filter { |file| html?(file) }
  end

  # Find all links in html_files
  # The value returned by this method is formatted like so:
  # {
  #   uri without fragment: {
  #     uri's fragment: Set [
  #       "file containing this link"
  #     ]
  #   }
  # }
  def links
    return @links if @links

    @links = {}
    html_files.each do |file|
      file_path = file_url(file)

      # For each link in the file
      uniq_file_matches(file, HREF).each do |link|
        uri = Addressable::URI.parse(link)

        # Skip the emails and phone numbers URIs
        next if uri.site&.end_with?(":")
        # Skip the URIs with unknown schemes
        next unless uri.scheme.nil? || SCHEMES.include?(uri.scheme)

        # Set the URI's path to the file's valid link if the link is a
        # fragment of the current file
        uri.path = file_path if link.start_with?("#")
        uri.path = uri.path.dup

        # Remove the fragment from the URI and put it in a local variable
        fragment = uri.fragment.nil? || uri.fragment.empty? ? nil : uri.fragment
        uri.fragment = nil

        fragment = nil if @ignore_fragments

        # Get the link for the URI
        uri_fragments = @links[uri] ||= {}

        # Get the files for the fragment
        fragment_files = uri_fragments[fragment] ||= Set.new

        fragment_files << file
      end
    end
    @links
  end

  private

  # Create a connection to make requests
  def create_connection
    Faraday.new do |faraday|
      faraday.use FaradayMiddleware::FollowRedirects
      faraday.use :cookie_jar
      faraday.adapter Faraday.default_adapter
    end
  end

  # Make a request on the connection for the URL
  def make_request(conn, url)
    if @mode != "get_only"
      response_status = head_request(conn, url).status
      return response_status if mode == "head_only" ||
                                status_allowed?(response_status)
    end

    get_request(conn, url).status
  end

  # Make a get request on the connection for the URL
  def get_request(conn, url)
    conn.get(url, {}, HEADERS)
  end

  # Make a head request on the connection for the URL
  def head_request(conn, url)
    conn.head(url, {}, HEADERS)
  end

  # Returns whether the status is successfull
  def status_allowed?(status)
    status >= 200 && status < 300
  end

  # Finds all the matches in a file for a given regex
  def uniq_file_matches(path, regex)
    uniq_string_matches(File.open(path).read, regex)
  end

  # Finds all the matches in a String for a given regex
  def uniq_string_matches(str, regex)
    str.scan(regex)
       .map { |matches| matches[0].strip }
       .uniq
  end

  # Determines whether the file is an HTML file based on it's extension
  def html?(path)
    HTML.include?(File.extname(path))
  end

  # Gets the url of a file in the static site based on its path
  def file_url(path)
    path = Pathname.new(path)
    path = path.relative_path_from(@site_folder)
    path = "/" + path.to_s
    path.chomp!("index.html")
    path.chomp!("/")
    path
  end
end
