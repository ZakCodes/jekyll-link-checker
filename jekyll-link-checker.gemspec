# frozen_string_literal: true

require_relative "lib/version"

Gem::Specification.new do |s|
  s.name        = "jekyll-link-checker"
  s.version     = JekyllLinkChecker::VERSION
  s.licenses    = ["MIT"]
  s.authors     = ["Zakary Kamal Ismail"]
  s.email       = "zakary.kamal.fs@outlook.com"
  s.homepage    = "https://gitlab.com/ZakCodes/jekyll-link-checker"
  s.summary     = "Checks all the links of a Jekyll website."
  s.description = "Verifies that all the links in a Jekyll website are valid." \
                  "It can also work with any static site generator."

  s.files       = Dir["lib/**"] + ["Gemfile"]
  s.executables = ["jekyll-link-checker"]
  s.bindir      = "exe"
  s.require_paths = ["lib"]

  s.metadata = {
    "bug_tracker_uri" =>
      "https://gitlab.com/ZakCodes/jekyll-link-checker/issues",
    "changelog_uri" =>
      "https://gitlab.com/ZakCodes/jekyll-link-checker/-/releases",
    "homepage_uri" => s.homepage,
    "source_code_uri" => "https://gitlab.com/ZakCodes/jekyll-link-checker"
  }

  s.add_dependency("faraday", "~> 0.17")
  s.add_dependency("faraday-cookie_jar", "0.0.6")
  s.add_dependency("faraday_middleware", "~> 0.13")
  s.add_dependency("jekyll", ">= 3.0", "< 5.0")

  s.add_development_dependency("rspec")
  s.add_development_dependency("rubocop")
end
