# Jekyll-link-checker
The link checker can be used and configured in 2 different ways: as a [Jekyll] plugin or as a CLI tool both with a config file. It can be used to check whether all the links, internal and external, in your HTML files are valid so your users never have to see a `404 Not Found` error. By default, it also checks whether the links to fragments are valid, but this can be disabled.  

## Installation
This gem is [published on RubyGems](https://rubygems.org/gems/jekyll-link-checker), so you can install it like so:
```sh
gem install jekyll-link-checker
```

You can also add it to your Gemfile and even use the latest development version from master or from your own fork like so:
```ruby
# From RubyGems
# You should specify a version
gem 'jekyll-link-checker'

# Development version from master
# You can specify your own git repository instead
gem 'jekyll-link-checker', git: 'https://gitlab.com/ZakCodes/jekyll-link-checker'
```

## \_config.yml or \_config.yaml
If you already use [Jekyll], the minimum necessary configuration for the `_config.yml` is probably already satisfied.  

See [Configuration](#configuration) for more info on the configuration options.

## CLI
The CLI tool allows you to check the links of a website independently from [Jekyll]. It can therefore be used for any static website. However, you'll have to pass a lot of arguments to the tool to configure it properly. To save you some time, the tool will look for files named `_config.yml` or `_config.yaml` and use them. Even if you use another static site generator, you can still add a config file to your site's folder so it can be used by the tool.

Execute `jekyll-link-checker --help` for information on how to use it.

## Configuration
Here is a description of the different configuration options, the default values and their descriptions.

Here is an example configuration using all the possible configuration options:
```yaml
url: https://example.com
baseurl: / # This option is currently ignored
destination: _site

link-checker:
  skip-list: []
  ignore-fragments: false
  mode: head_only
  verbose: false
  fail-fast: false
  abort: true
  allowed-status: # This option is currently ignored
    - [200, 300[
```

### allowed-status
default: 200 to 299 inclusively

### skip-list
default: []
The skip list must be either an array of string or a single string (not in an array). If it's an array, each array element will be interpreted as a link to ignore, otherwise the string will be interpreted as a filename and each line in the file will be read as a link to ignore.  
This option is really useful for websites that you know are valid, but return an error status code.

### mode
* (default) try_head: Tries to do a HEAD request, then a GET request if the HEAD request fails
* get_only: Only does a GET request
* head_only: Only does a HEAD request

### ignore-fragments
* (default) false
* true: Skips checking whether the link fragments are valid

## Dependencies
### [addressable](https://github.com/sporkmonger/addressable)
Used to parse links

### [faraday](https://github.com/lostisland/faraday)
Used to make requests

### [faraday_middleware](https://github.com/lostisland/faraday_middleware)
Used for the FaradayMiddleware::FollowRedirects middleware that follows HTTP redirects. However, it prevents us from making requests in parallel (but I'm not sure because we might be able to create multiple connections and then be able to use those connections in parallel).

### [faraday-cookie_jar](https://github.com/miyagawa/faraday-cookie_jar)
Used to persist cookies on redirects. This is required for certain sites like twitter.

[Jekyll]: https://jekyllrb.com/
